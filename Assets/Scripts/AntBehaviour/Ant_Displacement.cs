using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ant_Displacement : MonoBehaviour
{
    //Setup variable (velocity, float Max and Min, timing, angle)
    public float velocityMax;

    public float xMax;
    public float zMax;
    public float xMin;
    public float zMin;

    private float x;
    private float z;
    private float temp;
    private float angle;

    void Start()
    {
        //Define at start value for the x float and z float
        x = Random.Range(-velocityMax, velocityMax);
        z = Random.Range(-velocityMax, velocityMax);
        //Same for the angle
        angle = Mathf.Atan2(x, z) * (180 / 3.141592f) + 90;
        transform.localRotation = Quaternion.Euler(0, angle, 0);
    }

    //Object move with in one direction before the timing and the destination end, Object change and define new direction
    //Source : Unity forum
    void Update()
    {
        temp += Time.deltaTime;

        if (transform.localPosition.x > xMax)
        {
            x = Random.Range(-velocityMax, 0.0f);
            angle = Mathf.Atan2(x, z) * (180 / 3.141592f) + 90;
            transform.localRotation = Quaternion.Euler(0, angle, 0);
            temp = 0.0f;
        }
        if (transform.localPosition.x < xMin)
        {
            x = Random.Range(0.0f, velocityMax);
            angle = Mathf.Atan2(x, z) * (180 / 3.141592f) + 90;
            transform.localRotation = Quaternion.Euler(0, angle, 0);
            temp = 0.0f;
        }
        if (transform.localPosition.z > zMax)
        {
            z = Random.Range(-velocityMax, 0.0f);
            angle = Mathf.Atan2(x, z) * (180 / 3.141592f) + 90;
            transform.localRotation = Quaternion.Euler(0, angle, 0);
            temp = 0.0f;
        }
        if (transform.localPosition.z < zMin)
        {
            z = Random.Range(0.0f, velocityMax);
            angle = Mathf.Atan2(x, z) * (180 / 3.141592f) + 90;
            transform.localRotation = Quaternion.Euler(0, angle, 0);
            temp = 0.0f;
        }

        if (temp > 1.0f)
        {
            x = Random.Range(-velocityMax, velocityMax);
            z = Random.Range(-velocityMax, velocityMax);
            angle = Mathf.Atan2(x, z) * (180 / 3.141592f) + 90;
            transform.localRotation = Quaternion.Euler(0, angle, 0);
            temp = 0.0f;
        }

        transform.localPosition = new Vector3(transform.localPosition.x + x, transform.localPosition.y, transform.localPosition.z + z);
    }
}
