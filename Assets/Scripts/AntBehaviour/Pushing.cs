using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pushing : MonoBehaviour
{
    //Setup variable (rigidbody, explosion settings)
    public Rigidbody rb;
    public float explosionForce;
    public float explosionRadius;

    private void OnTriggerStay(Collider other)
    {
        //The object need to be on the specific layer
        if (other.gameObject.tag == this.gameObject.tag && other.gameObject.layer == 2)
        {
            Debug.Log("Gotcha !");
            //Fonction (using rigidbody) for push the ant
            CreateDirection(other.transform.position);
        }
    }

    private void CreateDirection(Vector3 target)
    {
        //Debug.DrawRay(transform.position, transform.TransformDirection(target), Color.green);
        rb.AddExplosionForce(explosionForce, target, explosionRadius);
    }
}
