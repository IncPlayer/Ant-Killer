using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerAnt : MonoBehaviour
{
    //Setup variable (array)
    public GameObject[] antPrefabs;
    public GameObject[] antSpawned;
    public int actualAntNumber;
    public int antNumber;

    public void Awake()
    {
        /*for (actualAntNumber = 0; actualAntNumber <= antNumber;)
        {
            Instantiate(antPrefabs[Random.Range(0, 4)], transform.position, Quaternion.identity);
            actualAntNumber += 1;
        }*/

        //Create Ant at the start of the game
        foreach (GameObject ant in antPrefabs)
        {
            for (int i = 0; i < antNumber; i++)
            {
                Instantiate(antPrefabs[i], transform.position, Quaternion.identity);
            }
        }
    }

    private void Start()
    {
        //Attribute the tag to the ant who are already in the scene
        antSpawned = GameObject.FindGameObjectsWithTag("Ant");

        for (int i = 0; i < antSpawned.Length; i++)
        {
            if(antSpawned[i].gameObject.name == "AntB(Clone)")
            {
                antSpawned[i].gameObject.tag = "Blue";
            }
            if (antSpawned[i].gameObject.name == "AntG(Clone)")
            {
                antSpawned[i].gameObject.tag = "Green";
            }
            if (antSpawned[i].gameObject.name == "AntR(Clone)")
            {
                antSpawned[i].gameObject.tag = "Red";
            }
            if (antSpawned[i].gameObject.name == "AntBL(Clone)")
            {
                antSpawned[i].gameObject.tag = "Black";
            }
        }
    }
}
