using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SetVolume : MonoBehaviour
{
    public AudioMixer mixer;

    // Method used to set the mixer's level to the slider's one
    // It permit to make a link between the the controller and the mixer... logarithmic way
    public void SetLevel(float sliderValue)
    {
        mixer.SetFloat ("Volume", Mathf.Log10 (sliderValue) * 20);
    }
}
