using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    public AudioClip red, blue, green, black, soundTransition;
    
    AudioSource soundManager;

    // Start is called before the first frame update
    void Start()
    {
        /*
        red = Resources.Load<AudioClip>("Red");
        blue = Resources.Load<AudioClip>("Blue");
        green = Resources.Load<AudioClip>("Green");
        black = Resources.Load<AudioClip>("Black");
        */
        
        soundManager = GetComponent<AudioSource>();    
    }

    //Gestion de la musique et des effets sonores
    public void PlaySound (string clip) 
    {
        switch (clip) 
        {
            case "Red":
                soundManager.PlayOneShot(red);
                break;

            case "Blue":
                soundManager.PlayOneShot(blue);
                break;

            case "Green":
                soundManager.PlayOneShot(green);
                break;
            case "Black":
                soundManager.PlayOneShot(black);
                break;

            case "soundTransition":
                soundManager.PlayOneShot(soundTransition);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
