using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationEvent : MonoBehaviour
{
    public ActDialog actDialog;
    public DialogData winCondition;
    public DialogData looseCondition;
    public SoundManager soundM;
    public static Animator animatorTransition;
    //startdialog is call by animation event transition
    private void Awake()
    {
        animatorTransition = GameObject.Find("transition").GetComponent<Animator>();
    }
    public void startDialogWin()
    {
        actDialog.StartDialog(winCondition);
    }
    public void statDialogLoose()
    {
        actDialog.StartDialog(looseCondition);
    }
    public void soundTransition()
    {
        if(soundM != null)
        {
            soundM.PlaySound("soundTransition");
        }
    }
}
