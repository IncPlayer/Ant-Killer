using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class initializer : MonoBehaviour
{
    public GameObject obj;

    private void Awake()
    {   
        //Search GameManager and destoy other GameManager if number of manager is superior of 1 and create new game manager if is number is equal 0 
        var tmp = GameObject.FindObjectsOfType<GameManager>();
       // Debug.Log(tmp.Length);
        if (tmp.Length > 1)
        {
            foreach (var item in tmp)
            {
                Debug.Log(item.name);
                Destroy(item);
            }
            Instantiate(obj);
        }
        else if (tmp.Length == 0)
        {
            Instantiate(obj);
        }

        Debug.Log(GameManager.Instance.gameObject.name);
    }

}
