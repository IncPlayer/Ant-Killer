using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public GameObject btnPlay;
    public void BtnPlay()
    {
        btnPlay.SetActive(false);
        animationEvent.animatorTransition.SetBool("transitionLoose", true);
    }
}
