using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//script for manage scenes
public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    private int indexScene;
    //Singleton
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("GameManager");
                    _instance = container.AddComponent<GameManager>();
                }
            }
            return _instance;
        }
    }

    void Awake()
    {
        //initialize build index
        indexScene = SceneManager.GetActiveScene().buildIndex;
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        //Check if input is down and call change scene function 
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            GameManager.Instance.nextScene();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            GameManager.Instance.previewScene();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            animationEvent.animatorTransition.SetBool("transitionLoose", true);
        }
    }
    //Call next scene
    public void nextScene()
    {
        if (indexScene + 1 < SceneManager.sceneCountInBuildSettings)
        {
            indexScene = indexScene + 1;
            SceneManager.LoadScene(indexScene);
        }else if(indexScene +1 == SceneManager.sceneCountInBuildSettings)
        {
            indexScene = 0;
            SceneManager.LoadScene(indexScene);
        }

    }
    //Call preview scene
    public void previewScene()
    {
        if (indexScene - 1 >= 0)
        {
            Debug.Log("non");

            indexScene = indexScene - 1;
            SceneManager.LoadScene(indexScene);
        }
    }
    //Call reaload scene
    public void realoadScene()
    {
        if(SceneManager.GetActiveScene().buildIndex == 0)
        {
            nextScene();
        }
        else
        {
            SceneManager.LoadScene(indexScene);

        }
    }
}
