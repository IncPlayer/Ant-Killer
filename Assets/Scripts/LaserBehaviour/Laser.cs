using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    //setup the value like Color and other parameter
    public GameObject[] laserColors;
    public int Select = 0;
    public GameObject laser;

    public Color RedLaser = Color.red;
    public Color BlueLaser = Color.blue;
    public Color GreenLaser = Color.green;
    public Color BlackLaser = Color.black;

    public GameObject prefabTest;
    public GameObject check;
    public GameObject checktake;

    float t = 0.0f;

    private void Update()
    {
        Vector3 clickPos = -Vector3.one;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            clickPos = hit.point;
        }

        LaserFollow(clickPos);

        //Instance of the laser
        if (Input.GetMouseButton(0))
        {
            GameObject test = Instantiate(laserColors[Select], clickPos, Quaternion.identity);
            laser.gameObject.SetActive(true);
        }
        else
        {
            laser.gameObject.SetActive(false);
        }

        //if (Input.GetMouseButtonDown(0))
        //{
            //GameObject checking = Instantiate(check, clickPos, Camera.main.transform.rotation);
            //checktake = GameObject.Find("check(Clone)");
        //}

        //Zoom(clickPos, zoomIn);

        //if(checktake != null)
        //{
            //checktake.transform.position = new Vector3(clickPos.x, checktake.transform.position.y, clickPos.z);
        //}

        //Input for change the color of the laser
        if (Input.GetMouseButtonDown(1))
        {
            SwitchColors();
        }

        if (Select == 0)
        {
            SetupVisuel(laser, BlueLaser);
        }
        if (Select == 1)
        {
            SetupVisuel(laser, GreenLaser);
        }
        if (Select == 2)
        {
            SetupVisuel(laser, RedLaser);
        }
        if(Select == 3)
        {
            SetupVisuel(laser, BlackLaser);
        }
    }

    //Follow the pointer when the player click
    private void LaserFollow(Vector3 orientationPos)
    {
        this.gameObject.transform.LookAt(orientationPos);
    }

    //Use the LineRenderer
    private void SetupVisuel(GameObject Line, Color color)
    {
        Line.GetComponent<LineRenderer>().SetColors(color, color);
    }

    private void SwitchColors()
    {
        Select += 1;

        if (Select == 4)
        {
            Select = 0;
        }
    }

    /*Zoom fonction
    private void Zoom(Vector3 clickPos, bool verif)
    {
        if (zoomIn)
        {
            Camera.main.transform.LookAt(new Vector3(checktake.transform.position.x, 0,checktake.transform.position.z)); ;
            Camera.main.fieldOfView = 50;
        }
        else
        {
            Camera.main.transform.rotation = rotationCamera;
            Camera.main.fieldOfView = 60;
        }
    }*/
}
