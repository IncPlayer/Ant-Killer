using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactBehaviour : MonoBehaviour
{
    public CompareNotes array;
    public int actualPlace = 0;

    public SoundManager soundM;

    private void Awake()
    {
        //Set the array and if the object is null
        if (array || soundM == null)
        {
            if(GameObject.Find("manage") != null)
            {
                array = GameObject.Find("manage").GetComponent<CompareNotes>();
                soundM = GameObject.Find("manage").GetComponent<SoundManager>();
            }
        }
    }

    private void Start()
    {
        //reset some value
        actualPlace = 0;
        StartCoroutine(DeadTiming(0.5f));
    }

    IEnumerator DeadTiming(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        //if the component are not null, Detect the hit on ant or other object
        if (other.gameObject.tag == this.gameObject.tag && other.gameObject.layer == 8)
        {
            Debug.Log(other.tag);
            Debug.Log(other.name);

            if(array != null)
            {
                array.antNotes[array.currentStateList] = other.gameObject.tag;
                array.verification();
            }

            other.transform.parent.gameObject.SetActive(false);

            if(soundM != null)
            {
                //Each ant have one sound
                if (other.gameObject.tag == "Red")
                {
                    Debug.Log("S_Red");
                    soundM.PlaySound("Red");
                }
                if (other.gameObject.tag == "Blue")
                {
                    Debug.Log("S_Blue");
                    soundM.PlaySound("Blue");
                }
                if (other.gameObject.tag == "Green")
                {
                    Debug.Log("S_Green");
                    soundM.PlaySound("Green");
                }
                if (other.gameObject.tag == "Black")
                {
                    Debug.Log("S_Black");
                    soundM.PlaySound("Black");
                }
            }

            Debug.Log("ok");
        }

        /*if(other.gameObject.tag == notes.antNotesRef)
        {
            currentTarget ++;
            Debug.Log("OUI !");
        }*/
    }

    // Method to play if the player wins
    /*void Victory()
    {
        Debug.Log("Bien joué, c'était la bonne combinaison !");
    }

    void CompareNotes()
    {
        // Compare the notes and increment if it's correct
        if(gameObject.tag == currentTarget)
        {
            currentTarget ++;
            Debug.Log("OUI !");
        }
    }*/
}
