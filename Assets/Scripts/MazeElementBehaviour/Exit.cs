using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{
    public int exitCount = 0;
    public Animator transition;

    public void OnTriggerEnter(Collider other)
    {
        exitCount++;
    }

    private void Update()
    {
        if(exitCount == 4)
        {
            transition.SetBool("transitionWin", true);
        }
    }
}
