using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateDoor : MonoBehaviour
{
    public GameObject[] doors;

    /*First idea verification by Tag
      Second idea verification by GameObject*/
    private void OnTriggerEnter(Collider other)
    {
        /* if (other.gameObject.CompareTag("Porte")){}*/
        foreach (GameObject door in doors)
        {      
            Destroy(door.gameObject);
        }
    }
}
