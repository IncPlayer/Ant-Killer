using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    public Animator transition;

    /*Ants collision Traps*/
    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(Death(other.gameObject));
    }

    IEnumerator Death(GameObject target)
    {
        transition.SetBool("transitionLoose", true);
        yield return new WaitForSeconds(1);
        Destroy(target.gameObject);
    }
}
