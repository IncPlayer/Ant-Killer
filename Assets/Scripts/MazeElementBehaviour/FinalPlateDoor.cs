using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalPlateDoor : MonoBehaviour
{
    public int plate = 0;
    public int plateObjective = 4;
    public bool verif;
    public bool block;
    public GameObject[] doors;
    private void OnTriggerEnter(Collider other)
    {
        /*Verification by Tag + counter*/

        if (other.gameObject.tag == this.gameObject.tag)
        {
            verif = true;
            plate++;
            /*foreach (GameObject door in doors)
            {
                Destroy(door.gameObject);
            }*/
        }
    }
    private void OnTriggerExit(Collider other)
    {
        verif = false;
    }

    private void goalcalcul()
    {
        if (plate == plateObjective)
        {
            verif = true;
        }
    }
}
