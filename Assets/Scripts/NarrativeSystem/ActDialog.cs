using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ActDialog : MonoBehaviour
{
    [Space]
    [Header("Variables Settings")]
    public bool readAuto;

    [Space]
    [Header("Data")]
    public DialogData dialogData;

    [Space]
    [Header("UserInterface Settings")]
    public GameObject narrationPanel;
    public TextMeshProUGUI textDisplay;
    private void Awake()
    {
        narrationPanel = GameObject.Find("narrationPanel");
        textDisplay = narrationPanel.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        narrationPanel.SetActive(false);
    }

    //Display and start the reading dialog
    public void StartDialog(DialogData newDialogData)
    {
        dialogData = newDialogData;
        dialogData.index = 0;
        narrationPanel.SetActive(true);
        StartCoroutine(Type());

    }
    //Fonction for typing the dialog (each letter in sentences with a specific timing)
    IEnumerator Type()
    {

        foreach (char letter in dialogData.sentences[dialogData.index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(dialogData.typingSpeed[dialogData.index]);
        }
        if (textDisplay.text == dialogData.sentences[dialogData.index])
        {
            if (readAuto)
            {
                yield return new WaitForSeconds(dialogData.waitingSpeed[dialogData.index]);
                NextSentence();
            }
            else
            {
                if (dialogData.index < dialogData.sentences.Length - 1)
                {
                    yield return new WaitForSeconds(dialogData.waitingSpeed[dialogData.index]);
                    textDisplay.text = "";
                    narrationPanel.SetActive(false);
                    dialogData.index++;
                }
                else
                {
                    yield return new WaitForSeconds(dialogData.waitingSpeed[dialogData.index]);
                    textDisplay.text = "";
                    narrationPanel.SetActive(false);

                }
            }
        }
    }

    //Fonction for read the entire dialog in one time
    public void NextSentence()
    {
        if (dialogData.index < dialogData.sentences.Length - 1)
        {
            dialogData.index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }
        else
        {
            textDisplay.text = "";
            narrationPanel.SetActive(false);
            if (dialogData.name.Contains("win"))
            {
                GameManager.Instance.nextScene();
            }
            else
            {
                GameManager.Instance.realoadScene();
            }
        }
    }

}

