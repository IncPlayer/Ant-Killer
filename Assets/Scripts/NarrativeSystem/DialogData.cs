using UnityEngine;


[CreateAssetMenu(fileName = "Dialog Data", menuName = "narrative Sytem/Data", order = 1)]

public class DialogData : ScriptableObject
{

    public string[] sentences;
    public float[] typingSpeed;
    public float[] waitingSpeed;
    public int index;

}
