using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// the script retrieves the enclosure and calculates if all the enclosures have the correct number of ants
public class LevelManagerEnclosure : MonoBehaviour
{
    public GameObject[] enclosure;
    private int numberEnclosure = 0;
    public int numberGoalAchieved = 0;
    public  Animator transition;
    public float timeReamaning = 60.0f;
    public TextMeshProUGUI timeDisplay;
    private bool block = false;
    void Start()
    {
        //take all item in array and increment value number enclosure
        foreach (GameObject go in enclosure)
        {
            numberEnclosure++;
        }
    }

    void Update()
    {
        //Detection if a number of ant is in enclosure
        foreach (GameObject go in enclosure)
        {
            if (go.GetComponent<Enclosure>().goalAchieved == true && go.GetComponent<Enclosure>().blockAchieved == false)
            {
                go.GetComponent<Enclosure>().blockAchieved = true;
                numberGoalAchieved++;
                transitionWinScene();
            }
            else if (go.GetComponent<Enclosure>().blockAchieved == true)
            {
                if (go.GetComponent<Enclosure>().countAnt != go.GetComponent<Enclosure>().goalObjective)
                {
                    numberGoalAchieved--;
                    go.GetComponent<Enclosure>().goalAchieved = false;
                    go.GetComponent<Enclosure>().blockAchieved = false;
                }
            }
        }
        //Timer
        timeReamaning -= Time.deltaTime;

        if (timeReamaning <= 0.0f && block == false)
        {
            gameOver();
        }
        else
        {
            float seconds = Mathf.FloorToInt(timeReamaning % 60);
            timeDisplay.text = "Temps restant : " + seconds.ToString();

        }
    }
    //transition win and game over and launch animation
    public void transitionWinScene()
    {
        if (numberEnclosure == numberGoalAchieved)
        {
            block = true;
            transition.SetBool("transitionWin", true);
        }
    }
    public void gameOver()
    {
        transition.SetBool("transitionLoose", true);
    }

}
