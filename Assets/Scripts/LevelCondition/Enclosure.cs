using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script for detect collision and calcul number of ant in the collider
public class Enclosure : MonoBehaviour
{
    public int countAnt = 0;
    public int goalObjective = 2;
    public bool goalAchieved = false;
    public bool blockAchieved = false;

    public bool stopAnt = false;

    //Detection if object have a collision and if that corresponds with object tag
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == gameObject.tag && other.gameObject.layer != LayerMask.NameToLayer("Default"))
        {
            //Debug.Log("oui" + other.gameObject.name);
            countAnt++;
            winCondition();
            stopAnt = true;
            StopAnt(other.gameObject);
            other.gameObject.layer = 0;
        }
    }

    //Feature cancel

    /*private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == gameObject.tag)
        {
            countAnt--;
            goalAchieved = false;
        }
    }*/

    /* Debug number count ant for test
    private void Update()
    {
        Debug.Log("number : " + countAnt);
    }*/

    //Comparison of number of ant is equal as goal objective
    private void winCondition()
    {
        if (countAnt == goalObjective)
        {
            goalAchieved = true;
        }
    }

    //Stop displacement randomly of ant
    private void StopAnt(GameObject target)
    {
        Debug.Log(target.name);
        Debug.Log(target.layer);
        target.GetComponent<Ant_Displacement>().enabled = false;
        target.GetComponent<Rigidbody>().useGravity = false;
        target.GetComponent<Rigidbody>().isKinematic = true;
    }
}
