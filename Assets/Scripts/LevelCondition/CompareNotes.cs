using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompareNotes : MonoBehaviour
{
    //2 Array to compare, the main and the second who need to be complete by the player
    public string[] antNotesRef;
    public string[] antNotes;

    //Index of Array
    public int currentStateList;

    public Animator transition;

    public void Start()
    {
        //Setup Array Length
        antNotes = new string[7];

        //Setup Index
        currentStateList = 0;
    }

    //Fonction called in other script to verifying if the string are the same
    public void verification()
    {
        if (antNotes[currentStateList] == antNotesRef[currentStateList])
        {
            Debug.Log("NICE !");
            currentStateList += 1;

            if(currentStateList == antNotes.Length)
            {
                Debug.Log("YES !");
                transition.SetBool("transitionWin", true);
            }
        }
        else
        {
            Debug.Log("NOT NICE !");

            for (int i = 0; i < antNotes.Length; i++)
            {
                antNotes[i] = null;
            }

            currentStateList = 0;
        }
    }
}
